# workstation role

Setup a computer as a workstation.

This involves installing a lot of applications, e.g.

- Firefox
- Emacs
- Gimp
- Scribus
- DigiKam
- VLC
- LibreOffice
- Signal
- QOwnNotes
- Several fonts
- Seafile

A few commands line utilities:

- yadm
- mosh
- tree

A few applications are installed as snap packages:

- Spotify
- BitWarden
- Chromium
- Skype

If the Desktop Environment is KDE, additionally a few packages are installed:

- muon
- libreoffice-kde
- libreoffice-style-breeze
- libreoffice-style-oxygen
- ktorrent
- kontact
- krdc
- akregator
- clementine
- cantata

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Dependencies

_This role does not depend on other Ansible roles._

## Role Variables

_This role has no variables._

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: workstation
```
